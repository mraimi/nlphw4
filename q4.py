#! usr/bin/python
import sys, os
from subprocess import PIPE, Popen
from collections import defaultdict
import StringIO
import re

class GLM:
	def __init__(self):
		self.tags = defaultdict(int)
		self.suff = defaultdict(int)

	def read(self):
		tm = open('tag.model','r')
		for line in tm:
			tmp = line.strip().split()
			self.tags[tmp[0]] = float(tmp[1])

	def enum_hist(self):
		dev = open('tag_dev.dat','r')
		dev2 = open('tag_dev.dat','r')
		out = open('tag_dev.out','w')
		weightedHist = ""
		array = []
		p1 = Popen(["sudo","python","tagger_history_generator.py","ENUM"], stdin=dev, stdout=PIPE)

		sent_count = 0
		hist_ex_count = 1
		ind_hist = 0
		weight = 0
		dev2.seek(0)
		w = ""
		while True:
			if len(array)==0: 		#need a new sentence
				while w != "\n": 	#not at end of sentence
					array.append(w.strip()) #keep this order
					w = dev2.readline()
				sent_count += 1
			ind_hist += 1
			hist = p1.stdout.readline()
			spl = hist.strip().split()
			if hist != "":				#not at eof
				if hist == "\n": 		#end of example's histories
					hist_ex_count += 1
					weightedHist += "\n"
					array = []
					w = dev2.readline()
					if w == "":
						p2 = Popen(["sudo", "python", "tagger_decoder.py", "HISTORY"], stdin=PIPE, stdout=PIPE)
						out.write(p2.communicate(weightedHist)[0].decode())
						break
					continue	
				for word in array:
					weight += self.tags["BIGRAM:"+spl[1]+":"+spl[2]]
					weight += self.tags["TAG:"+array[int(spl[0])-1]+":"+spl[2]]
					for i in range(1,4):
						weight += self.suff["SUFF:"+self.suffix(array[int(spl[0])-1],i)+":"+str(i)+":"+spl[2]]
				weightedHist += hist.strip() +" "+ str(weight)+"\n"
				weight = 0			#reset for next history
			else:	#eof
				break
		

	def score(self):
		tags = open('tag_dev.out','r')
		sentences = open('tag_dev.dat','r')
		o = open('toScore.dat','w')
		while True:
			t = tags.readline().strip().split()
			s = sentences.readline()
			if s == "":
				break
			if s == "\n":
				t = tags.readline()
				o.write("\n")
				continue 
			s = s.strip()
			try:
				o.write(s +" "+t[2]+"\n")
			except IndexError:
				print ""

	def suffix(self, word, j):
		return word[len(word)-j:len(word)]


	def perceptron(self, iterations):
		train = open('tag_train.dat','r')
		train2 = open('tag_train.dat','r')
		train3 = open('tag_train.dat','r')
		train4 = open('tag_train.dat','r')

		weightHist = ""
		gold = Popen(["sudo","python","tagger_history_generator.py","GOLD"], stdin=train, stdout=PIPE)
		enum = Popen(["sudo","python","tagger_history_generator.py","ENUM"], stdin=train2, stdout=PIPE)
		count = 0
		s_array = []
		for x in range (1,iterations+1):
			count += 1
			while True:
				if len(s_array) == 0: 						#needs new sentence
					w = train3.readline().split('\t')[0]	#get new sentence
					while w != "\n": 						#not at end of sentence
							if w == "":
								break
							s_array.append(w.strip().split('\t')[0])
							w = train3.readline()
				e = enum.stdout.readline()					#get new history
				if (e == ""):								#eof
					break
				if (e == '\n'):								#end of sentence histories
					s_array = []
					weightHist += "\n"
					continue						
				spl = e.strip().split()						#split history
				word_num = int(spl[0])-1					#find index to word in history
				weight = 0
				for i in range(1,4):
					weight += self.suff["SUFF:"+self.suffix(s_array[word_num],i)+":"+str(i)+":"+spl[2]]
				weightHist += e.strip() + " " + str(weight) + "\n"
				weight = 0
			hist = Popen(["sudo", "python", "tagger_decoder.py", "HISTORY"], stdin=PIPE, stdout=PIPE)
			results = hist.communicate(weightHist)[0].decode()
			results = StringIO.StringIO(results)
			# NOW UPDATE v #
			
			s_array = []
			w = ""
			while True:
				if len(s_array) == 0: 						#needs new sentence
					w = train4.readline().split('\t')[0]	#get new sentence
					while w != "\n": 						#not at end of sentence
							if w == "":
								break
							s_array.append(w.strip().split('\t')[0])
							w = train4.readline()
				g = gold.stdout.readline()
				z = results.readline().strip().split()
				if g == "\n":
					z = results.readline()					#compensates for gold not having stop
					s_array = []
					continue
				if g == "":
					break
				g = g.strip().split()
				curr_word = s_array[int(g[0])-1]
				if (unicode(g[1])!=z[1]) or (unicode(g[2])!=z[2]):
					# print "g1:" + g[1] + "z1:" + z[1] +"|"
					# print "g2:" + g[2] + "z2:" + z[2] +"|"
					for i in range(1,4):
						self.suff["SUFF:"+self.suffix(curr_word,i)+":"+str(i)+":"+g[2]] += 1
						self.suff["SUFF:"+self.suffix(curr_word,i)+":"+str(i)+":"+z[2]] -= 1
g = GLM()
g.perceptron(5)
g.read()
g.enum_hist()
g.score()


